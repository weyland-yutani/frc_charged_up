# Coral Edge TPU Dev Board
The link to setting up the Coral Edge TPU board can be found here:
https://coral.ai/docs/dev-board/get-started/#requirements

I will highlight the steps below and include notes where appropriate.

## What you need
- One microSD card with at least 8 GB capacity
- One USB-C power supply (2-3 A / 5 V)
- One USB hub for keyboard keybaord and mouse unless you plan to ssh into the board
- HDMI cable

## Flashing the mircoSD card
1. Download the image from here [Mendel Linux](https://dl.google.com/coral/mendel/enterprise/enterprise-eagle-flashcard-20211117215217.zip)
2. Unzip the image
3. I used [balena etcher](https://www.balena.io/etcher/) to burn the _.img_ file to the SD card
4. Set the boot mode switches to _SD card_ mode

| Boot mode | Switch 1 | Switch 2 | Switch 3 | Switch 4 |
| --- | --- | --- | --- | --- |
| SD card | ON | OFF | ON | ON |

I used a small screwdriver to flip the switches.
![Boot switches set to SD card mode](https://coral.ai/static/docs/images/devboard/devboard-bootmode-sdcard.jpg "SD Card Mode")
[Source](https://coral.ai/static/docs/images/devboard/devboard-bootmode-sdcard.jpg)

5. Insert the SD card and power on the device. I connected an HDMI cable to monitor progress. When its finished, the board powers down.
6. Remove SD card and disconnect power.
7. Set the boot mode switches to _eMMC_ mode

| Boot mode | Switch 1 | Switch 2 | Switch 3 | Switch 4 |
| --- | --- | --- | --- | --- |
| eMMC | ON | OFF | OFF | OFF |

Again, I used a small screwdriver to flip the switches.
![Boot switches set to eMMC mode](https://coral.ai/static/docs/images/devboard/devboard-bootmode-emmc.jpg "eMMC Mode")
[Source](https://coral.ai/static/docs/images/devboard/devboard-bootmode-emmc.jpg)

8. You are ready to power on your Coral Edge TPU Dev Board
9. Note, Mendel Linux has a GUI but the only application is a terminal.
10. To poweroff run
`sudo shutdown`

## MDT
The _Getting Started_ section proceeds to give instructions on how to install and run MDT. I skipped these steps since I plan to develop directly on the board. Instead, I attached the HDMI cable, and USB hub.

## Connecting to WIFI
To connect to wifi, I used the following command
`nmcli dev wifi connect <NETWORK_NAME> password <PASSWORD> ifname wlan0`
You can verify the connection with the following command
`nmcli connection show`
Note, you should only have to run this once.

## Update Mendel
`sudo apt update && sudo apt dist-upgrade`

## Running the demo app
If you have an HDMI cable attached you can run the demo using the command
`edgetpu_demo --device`
Note, the demo opens a maximized GUI window. To get back to your shell press the **Winkey + TAB**

# Generate Training Data
Note, I did this on a separate computer from the Coral Edge TPU Dev Board.

Note, for the training data included in this repo I used the images from this [repository](https://github.com/thompsonevan/chargedUpObjectDetection/tree/master/imgs) 

To generate the training model we will be using the site [Teachable Machine](https://teachablemachine.withgoogle.com/).

1. Press _Get Started_
2. Select _Image Project_
3. Select _Standard image model_
4. I choose to _upload images_
5. For Class 1, I choose images 0-500 (mostly yellow cone)
6. For Class 2, I choose images 500-946 (mostly purple cube)

Note, I highly recommend generating your own images. If possible, use the same camera that will be used on the robot. I would record video files of the objects at varying distances, varying orientations and at varying light levels.

Note, It is also worthwhile to manually go through your images and remove any that have motion blur.

Note, you can use ffmpeg to extract images from video.

`ffmpeg -i inputfile.avi -r 1 -f image2 image-%3d.jpeg`

7. Next press _Train Model_

Note, I usually select 10 or less epochs in the _advanced_. A too high of a value will overfit and decrease accuracy.

Note, while training *DO NOT* switch to a new tab, else the training will stop.

8. Next you can test your data by using a webcam or uploading sample images, or you can click _Export Model_.

9. Be sure to select _Tensor Flow Lite & Edge TPU_ and click _Download My Model_

10. Congrats, you now have your very own training data or you can use the training data in this repo _model_edgetpu.tflite_. The downloaded zip file should contain two files. a _txt_ labels file and the _tflite_ model.

# Mounting a USB device
I did the above training on a separate computer and as a result, I needed to copy it over to the Coral Edge TPU Dev Board.
1. Plug in Thumbdrive to Coral Edge TPU Dev Board
2. run `lsblk` to get drive. In my case it was /dev/sda
3. mount drive with the following command 

`sudo mount /dev/sda /mnt/USB`

4. Copy over necessary files
5. To unmount run

`sudo umount /mnt/USB`

# PyCoral
Clone and install PyCoral on the Coral Edge TPU Dev Board
```
mkdir coral && cd coral
git clone https://github.com/google-coral/pycoral.git
cd pycoral/
bash examples/install_requirements.sh
```

The following command can be used to test individual images with your model.
```
python3 examples/classify_image.py \
--model /home/mendel/Downloads/model_edgetpu.tflite \
--labels /home/mendel/Downloads/labels.txt \
--input /home/mendel/Downloads/frc_cube_1.jpg
```

The following command can be used to test the model with a USB camera attached to the Coral Edge TPU Dev Board.
```
edgetpu_classify \
--source /dev/video1:YUY2:640x480:30/1  \
--model /home/mendel/Downloads/model_edgetpu.tflite \
--labels /home/mendel/Downloads/labels.txt 
```

Note, for the USB camera, you can check the resolution and frame rate of the attached camera with this command

`v4l2-ctl --list-formats-ext --device /dev/video1`

A sample python launch file is also included. Please remember to edit modelPath and labelPath. These are presently hardcoded in the script.

Note, to run the python script I had to upgrade _pip_

`python3 -m pip install --upgrade pip`